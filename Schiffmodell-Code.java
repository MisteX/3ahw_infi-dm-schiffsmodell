import java.sql.*;

public class SQLiteJDBC 
{
	public static void main (String args [])
	{
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:schiffsmodell.db");
			System.out.println("Opened database successfully\n");
			
			stmt = c.createStatement();
			
			String sql_del_Schiff="drop table Schiff";
			String sql_del_Besatzung="drop table Besatzung";
			String sql_del_Tech_Sys="drop table Technische_Systeme";
			String sql_del_Komponenten="drop table Komponenten";
			String sql_del_Scheinwerfer="drop table Scheinwerfer";
			String sql_del_Bewaffnung="drop table Bewaffnung";
			String sql_del_TechnischeBewaffnung="drop table TechnischeBewaffnung";
			String sql_del_TechnischeBesatzung="drop table TechnischeBesatzung";
			String sql_del_TechnischeKomponenten="drop table TechnischeKomponenten";
			String sql_del_TechnischeScheinwerfer="drop table TechnischeScheinwerfer";
			
			stmt.executeUpdate(sql_del_Schiff);
			stmt.executeUpdate(sql_del_Besatzung);
			stmt.executeUpdate(sql_del_Tech_Sys);
			stmt.executeUpdate(sql_del_Komponenten);
			stmt.executeUpdate(sql_del_Scheinwerfer);
			stmt.executeUpdate(sql_del_Bewaffnung);
			stmt.executeUpdate(sql_del_TechnischeBewaffnung);
			stmt.executeUpdate(sql_del_TechnischeBesatzung);
			stmt.executeUpdate(sql_del_TechnischeKomponenten);
			stmt.executeUpdate(sql_del_TechnischeScheinwerfer); 
			
			String sql = "CREATE TABLE if not exists Schiff " +
						" (nameS         VARCHAR(20)    PRIMARY KEY     NOT NULL," +
						" schiffsklasse  TEXT    NOT NULL, " + 
						" baudatum       TEXT    NOT NULL);";
						
			System.out.println("created Schiff successfully");
			stmt.executeUpdate(sql);
			String sql1 = "CREATE TABLE if not exists Besatzung"
					+ "(nameB TEXT,"
					+ " Rang TEXT,"
					+ " FOREIGN KEY (nameB) references Schiff (nameS));";
						
			System.out.println("created Besatzung successfully");
			stmt.executeUpdate(sql1);

		    String sql2 = "CREATE TABLE if not exists Technische_Systeme"
		    		+ "(anschaffungsdatum TEXT,"
		    		+ " seriennummer INTEGER PRIMARY KEY NOT NULL, "
		    		+ " FOREIGN KEY (seriennummer) references Schiff (nameS));";
			System.out.println("created Technische_Systeme successfully");
			stmt.executeUpdate(sql2);

			String sql12 = "CREATE TABLE if not exists TechnischeBesatzung "
					+ "(seriennummerTS INTEGER NOT NULL,"
					+ " nameB TEXT NOT NULL, "
					+ " FOREIGN KEY (seriennummerTS) references Technische_Systeme (seriennummer),"
					+ " FOREIGN KEY (nameB) references Besatzung (nameB),"
					+ " PRIMARY KEY(nameB, seriennummerTS));";
		System.out.println("created TechnischeBesatzung successfully");
		stmt.executeUpdate(sql12);

			
			String sql3 = "CREATE TABLE if not exists Komponenten " +
					"(seriennummer   INTEGER     PRIMARY KEY     NOT NULL," +
					" verwendung     TEXT    NOT NULL);";
		System.out.println("created Komponenten successfully");
		stmt.executeUpdate(sql3);
		
		String sql23 = "CREATE TABLE if not exists TechnischeKomponenten " +
				"(seriennummerK     INTEGER     NOT NULL," +
				" seriennummerTS    TEXT    	NOT NULL, " + 
				" FOREIGN KEY (seriennummerTS) references Technische_Systeme (seriennummer)"
			  + " FOREIGN KEY (seriennummerK) references Kompenenten (seriennummer), "
			  + "PRIMARY KEY (seriennummerK, seriennummerTS));";
	System.out.println("created TechnischeKomponenten successfully");
	stmt.executeUpdate(sql23);

			String sql4 = "CREATE TABLE if not exists Bewaffnung " +
						"(anzahl         INTEGER     NOT NULL," +
						" seriennummer	 INTEGER 	 NOT NULL, "
					  + " geschuetznummer INTEGER	 NOT NULL," + 
					    "PRIMARY KEY(seriennummer, geschuetznummer));";
			System.out.println("created Bewaffnung successfully");
			stmt.executeUpdate(sql4);
			
			String sql24 = "CREATE TABLE if not exists TechnischeBewaffnung "
					+ "(seriennummerTS INTEGER, "
					+ "seriennummerB INTEGER,"
					+ "geschuetznummer INTEGER,"
					+ "FOREIGN KEY (geschuetznummer, seriennummerB) references Bewaffnung (geschuetznummer,seriennummerB),"
					+ "FOREIGN KEY (seriennummerTS) references Technische_Systeme (seriennummer), "
					+ "PRIMARY KEY (geschuetznummer, seriennummerTS, seriennummerB));";
		System.out.println("created TechnischeBewaffnung successfully");
		stmt.executeUpdate(sql24);
			
			String sql5 = "CREATE TABLE if not exists Scheinwerfer " +
						"(reichweite     REAL    	NOT NULL," +
						" anzahl         INTEGER 	NOT NULL," + 
						" groesse        REAL    	NOT NULL," +
					    " seriennummer   INTEGER 	NOT NULL," +
					    " scheinwerfernr INTEGER 	NOT NULL," +
					    " PRIMARY KEY (seriennummer, scheinwerfernr));";
			System.out.println("created Scheinwerfer successfully");
			stmt.executeUpdate(sql5);
			

			String sql25 = "CREATE TABLE if not exists TechnischeScheinwerfer " +
					"(seriennummerTS   INTEGER     NOT NULL," +
					" seriennummerS    TEXT    	   NOT NULL, " + 
					" scheinwerfernr   INTEGER	   NOT NULL, " +
					" FOREIGN KEY (seriennummerTS) references Technische_Systeme (seriennummer)"
				  + " FOREIGN KEY (seriennummerS, scheinwerfernr) references Scheinwerfer (seriennumer, scheinwerfernr),"
				  + " PRIMARY KEY (seriennummerTS, seriennummerS, scheinwerfernr));";
		System.out.println("created TechnischeScheinwerfer successfully");
		stmt.executeUpdate(sql25);
			
			stmt.close();
			c.close();
			
	} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
    }
		System.out.println("\nAll tables created successfully");
	}
}
